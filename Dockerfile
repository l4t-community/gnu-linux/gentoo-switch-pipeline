FROM arm64v8/ubuntu as base

ENV INIT_SYSTEM=${INIT_SYSTEM:-"openrc"}
ENV DESKTOP=${DESKTOP:-""}
ENV WITH_CUDA=${WITH_CUDA:-""}
ENV CI_PROJECT_DIR=${CI_PROJECT_DIR:-"/root"}

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update  -y && \
	apt install -y arch-install-scripts wget xz-utils

WORKDIR /root
ADD assets ./assets
ADD scripts ./scripts

CMD ./scripts/build
